
resource "aws_instance" "ec2" {
  ami           = var.ami
  count = 2
  instance_type = var.instancetype
  subnet_id     = var.subnet
  security_groups = ["sg-48aa892e"]
  key_name = aws_key_pair.ec2-keypair.id

  tags = {
    Name = "my-ec2.${count.index+1}"
  }
}

#keypair creation
resource "aws_key_pair" "ec2-keypair" {
  key_name   = "poojaec2"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDU8XE38kiPHnvkFwrrS/f/F/f/KBloPymHB2/JDkR1LS13RUMHPbOnO4t6zb0yZidmHEea1b7P9aTkiN00zJYS9gq0U/3891iCVsoO+QC4BQXDQRy1+14d7KVXrMTZae2b4SvxR9zQcYsOv0U3RS5kDNcH5NU6MayefM31ijtwNktuCVtoJLcmh0o/HTFmMlfUVTAxUmNKWcWmMd5WTqan4c/EegXLgHe6Rk82Ql+rBC3UpoguDb778IrwvmjbuhR83XEiEd52r+CwbLVguKMzGpw7lGAzsMsZxKor2MP+mFRecv2nk87dKeJl9QsvrDg35lQ/o7Z3CunlYHuE0pit1NSx0XLpEffGLBxcqh+VeY6RLV+7HV+zFnvihQ1dku00HcqLVuKh0HBj/4p5aF7hpp7wxrQOGASTRqOQX85WfFARocSdd+L9SBRfPeP8qNiaJNjb+TKdyE9hqkRdfjiQ/5Zr7q4587HcxSWtEsmvi6PBkx4oamhmUtRLFPf0UHk= ashok@DESKTOP-FC8BJQQ"
}
