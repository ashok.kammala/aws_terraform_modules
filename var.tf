
variable "region" {
  type = string
  default = "ap-south-1"
}
variable "ami" {
  type = string
  default = "ami-013168dc3850ef002"
}
variable "instancetype" {
  type = string
  default = "t2.micro"
}
variable "subnet" {
  type = string
  default = "subnet-8ab1a1e2"
}
